#include "packet.h"
#include <string>
#include <fstream>
#include <vector>

using namespace std ;

///
/// ReadFile - Function that reads a file and return a boolean
/// that determines if the file was succesfully opened and read or not.
/// fname - String that contains the filename of the file.
/// netdata - Vector of packets.
///
bool ReadFile(string fname, vector<Packet> & netdata){

    ifstream infile;
    string sa ;
    string da ;
    int sp ;
    int dp ;
    int oct ;
    int pkt ;

    infile.open(fname.c_str());

    if(!infile.is_open())
       return false ;

    while(infile>> sa >> da >> sp >> dp >> oct >>pkt ){
        Packet p(sa, da, sp, dp, oct, pkt) ;
        netdata.push_back(p) ;
    }

    infile.close() ;
    return true ;
}

///
/// SaveFile - Function that saves a file to the computer.
/// It returns true when the file was succesfully saved and false
/// if an error occurred.
/// fname - string that contains the filename of the file.
/// netdata - Vector of packets.
///
bool SaveFile(string fname, vector<Packet> netdata){

    ofstream outfile;

    outfile.open(fname.c_str());

    if(!outfile.is_open())
       return false ;

    for(int i = 0; i < netdata.size(); i++){
        outfile << netdata.at(i).getSrcAddr() << " " << netdata.at(i).getDstAddr() << " "
                << netdata.at(i).getSrcPort() << " " << netdata.at(i).getDstPort() << " "
                << netdata.at(i).getOctects() << " " << netdata.at(i).getPackets() << endl ;
    }

    outfile.close() ;
    return true ;
}
