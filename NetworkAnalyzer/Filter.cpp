#include <vector>
#include "packet.h"

///
/// FilterBySrcAddr - Function that filters the packets in
/// the netflow by the source address
/// netdata - Packet vector
/// address - string that represents the source address
///

void FilterBySrcAddr(vector<Packet> &netdata, string address){
    
    
}

///
/// FilterByDstAddr - Function that filters the packets in
/// the netflow by the destination address
/// netdata - Packet vector
/// address - string that represents the destination address
///

void FilterByDstAddr(vector<Packet> &netdata, string address){
  
    
}

///
/// FilterBySrcPort - Function that filters the packets in
/// the netflow by the source port
/// netdata - Packet vector
/// port - int that represents the source port
///

void FilterBySrcPort(vector<Packet> &netdata, int port){
   
    
}

///
/// FilterByDstPort - Function that filters the packets in
/// the netflow by the destination port
/// netdata - Packet vector
/// port - int that represents the destination port
///

void FilterByDstPort(vector<Packet> &netdata, int port){
    

}
