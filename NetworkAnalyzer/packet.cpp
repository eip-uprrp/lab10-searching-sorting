#include "packet.h"


///
/// Default constructor. The properties of the packet are set as follows:
/// * src_addr: set to the empty string.
/// * dst_addr: set to the empty string.
/// * src_port: set to 0.
/// * dst_port: set to 0.
/// * octects: set to 0.
/// * packets: set to 0.
/// * enabled: set to false.
Packet::Packet(){
    src_addr = "" ;
    dst_addr = "" ;
    src_port = 0 ;
    dst_port = 0 ;
    octects = 0 ;
    packets = 0 ;
    enabled = false ;
}

///
/// Constructor which accepts specification for sa(src_addr),
/// da(dst_addr), sp(src_port), dp(dst_port), oct(octects) and
/// pkt(packets)
///
Packet::Packet(string sa, string da, int sp, int dp, int oct, int pkt){
    src_addr = sa ;
    dst_addr = da ;
    src_port = sp ;
    dst_port = dp ;
    octects = oct ;
    packets = pkt ;
    enabled = true ;
}

///
/// Getter for the src_addr
///
string Packet::getSrcAddr(){
    return src_addr ;
}

///
/// Getter for the dst_addr
///
string Packet::getDstAddr(){
    return dst_addr ;
}

///
/// getter for the src_port
///
int Packet::getSrcPort(){
    return src_port ;
}

///
/// Getter for the dst_port
///
int Packet::getDstPort(){
    return dst_port ;
}

///
/// Getter for the octects
///
int Packet::getOctects(){
    return octects ;
}

///
/// Getter for the packets
///
int Packet::getPackets(){
    return packets ;
}

///
/// Setter for the src_addr
///
void Packet::setSrcAddr(string addr){
    src_addr = addr ;
}

///
/// Setter for the dst_addr
///
void Packet::setDstAddr(string addr){
    dst_addr = addr ;
}

///
/// Setter for the src_port
///
void Packet::setSrcPort(int port){
    src_port = port ;
}

///
/// Setter for the dst_port
///
void Packet::setDstPort(int port){
    if(port < 0)
        port = 0 ;
    else
        dst_port = port ;
}

///
/// Setter for the octects
///
void Packet::setOctects(int val){
    if(val < 0)
        octects = 0 ;
    else
        octects = val ;
}

///
/// Setter for the packets
///
void Packet::setPackets(int val){
    if(val < 0)
        packets = 0 ;
    else
        packets = val ;
}

///
/// Setter for the enabled variable.
/// Sets the value to true.
///
void Packet::enable(){
    enabled = true ;
}

///
/// Setter for the enabled variable.
/// Sets the value to false.
///
void Packet::disable(){
    enabled = false ;
}

///
/// Getter for enabled variable.
///
bool Packet::isEnabled(){
    return enabled ;
}
