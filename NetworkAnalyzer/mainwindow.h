#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <iostream>
#include <fstream>
#include <QComboBox>
#include <QMenuBar>
#include <QLabel>
#include <QTableWidget>
#include <QCheckBox>
#include <QLineEdit>
#include <QFileDialog>
#include <QMessageBox>
#include <QApplication>
#include "packet.h"

using namespace std;

namespace Ui {
class MainWindow;
}

bool ReadFile(string, vector<Packet> &) ;
bool SaveFile(string, vector<Packet>) ;

void FilterBySrcAddr(vector<Packet> &, string );
void FilterByDstAddr(vector<Packet> &, string );
void FilterBySrcPort(vector<Packet> &, int) ;
void FilterByDstPort(vector<Packet> &, int) ;

void SortBySrcAddr(vector<Packet> &) ;
void SortByDstAddr(vector<Packet> &) ;
void SortBySrcPort(vector<Packet> &) ;
void SortByDstPort(vector<Packet> &) ;

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    int numPackets, numOctects;
    int filterMethod;
    vector< Packet > netdata;   // Used to store the data from the network file
    QTableWidgetItem *item;      //This pointer is used to create table entries and add them to the table

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_Filter_Box_currentIndexChanged(int index); //

 //   void on_lineEdit_textChanged(const QString &arg1); //

    void on_actionLoad_Network_Data_triggered(); //

    void on_actionAbout_triggered(); //

    void on_actionExit_triggered();

    void FillTable(); //

    void on_runFilter_clicked();

    void on_pushButton_clicked();

    void on_sortButton_clicked();

    void on_openFile_clicked();

    void on_closeFile_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
