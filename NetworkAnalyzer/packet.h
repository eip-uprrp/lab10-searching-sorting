#ifndef PACKET_H
#define PACKET_H
#include <string>

using namespace std ;

/// A class to represent packets.
///
/// The class contains two constructurs and various functions
/// for searching and filtering the packets.
///
class Packet{
    public:
        Packet();
        Packet(string, string, int, int, int, int) ;
        string getSrcAddr() ;
        string getDstAddr() ;
        int getSrcPort() ;
        int getDstPort() ;
        int getOctects() ;
        int getPackets() ;
        void setSrcAddr(string) ;
        void setDstAddr(string) ;
        void setSrcPort(int) ;
        void setDstPort(int) ;
        void setOctects(int) ;
        void setPackets(int) ;
        void enable() ;
        void disable() ;
        bool isEnabled() ;

    private:
        string src_addr ;
        string dst_addr ;
        int src_port ;
        int dst_port ;
        int octects ;
        int packets ;
        bool enabled ;
};

#endif // PACKET_H
