// [RAN 2014-06-02] Fixed switch statements.

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    filterMethod = 0 ;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Filter_Box_currentIndexChanged(int index)
{
    filterMethod = index ;
    ui->lineEdit->clear();   
}


void MainWindow::on_actionLoad_Network_Data_triggered()
{
    QString fname = QFileDialog::getOpenFileName(this, tr("Choose a file"), QDir::currentPath());
    if (!fname.isEmpty())
    {
        if(!ReadFile(fname.toStdString(), netdata))
            netdata.clear();

        FillTable();
    }
}

void MainWindow::on_actionAbout_triggered()
{
    //display a message box
    QMessageBox::about(this, tr("About Network Traffic Analyzer"),
    tr("Network Traffic Analyzer was created with Qt Creator 2.7.0 (Qt 5.01 framework)"
       "\n\nCreated by Jonathan Velez Alvarez"
       "\n\nThe program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY "
       "OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE."));
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::FillTable()
{
    numPackets = 0;
    numOctects = 0;

    ui->table->setRowCount(0);
    for(int i=0; i<netdata.size(); i++){

        if (!netdata[i].isEnabled()){
            continue ;
        }
        ui->table->insertRow(ui->table->rowCount());
        numPackets += netdata[i].getPackets() ;
        numOctects += netdata[i].getOctects() ;
        // cout << netdata[i].getSrcPort() << endl ;
        ui->table->setItem(ui->table->rowCount()-1,0, new QTableWidgetItem(QString::fromStdString(netdata[i].getSrcAddr()))) ;
        ui->table->setItem(ui->table->rowCount()-1,1, new QTableWidgetItem(QString::fromStdString(netdata[i].getDstAddr()))) ;
        ui->table->setItem(ui->table->rowCount()-1,2, new QTableWidgetItem(QString::number(netdata[i].getSrcPort()))) ;
        ui->table->setItem(ui->table->rowCount()-1,3, new QTableWidgetItem(QString::number(netdata[i].getDstPort()))) ;
        ui->table->setItem(ui->table->rowCount()-1,4, new QTableWidgetItem(QString::number(netdata[i].getOctects()))) ;
        ui->table->setItem(ui->table->rowCount()-1,5, new QTableWidgetItem(QString::number(netdata[i].getPackets()))) ;

    }


    ui->Octets_Label->setText("Octets: "+QString::number(numOctects));
    ui->Packets_Label->setText("Packets: "+QString::number(numPackets));

    if(ui->table->rowCount()>0)
    {
        ui->Octets_Average_Label->setText("Octets Average: "+QString::number(float(numOctects/ui->table->rowCount())));
        ui->Packets_Average_Label->setText("Packets Average: "+QString::number(float(numPackets/ui->table->rowCount())));
    }
    else
    {
        ui->Octets_Average_Label->setText("Octets Average: ERROR! ");
        ui->Packets_Average_Label->setText("Packets Average: ERROR! ");
    }
}

//================================================================================================================

void MainWindow::on_runFilter_clicked()
{
    //cout << "FILTER" << filterMethod <<endl ;
    if(ui->lineEdit->text().length() < 1)
        return ;
    switch(filterMethod){
        case 0:
            FilterBySrcAddr(netdata, ui->lineEdit->text().toStdString());
            break ;
        case 1:
            FilterByDstAddr(netdata, ui->lineEdit->text().toStdString());
            break ;
        case 2:
            FilterBySrcPort(netdata, ui->lineEdit->text().toInt()) ;
            break ;
        case 3:
            FilterByDstPort(netdata, ui->lineEdit->text().toInt()) ;
            break ;
        default:
            break ;

    } ;
    FillTable() ;
}

void MainWindow::on_pushButton_clicked()
{
    for(unsigned int i = 0; i < netdata.size(); i++)
        netdata[i].enable() ;
    FillTable() ;
}

void MainWindow::on_sortButton_clicked()
{

    switch(filterMethod){
        case 0:
            SortBySrcAddr(netdata);
            break ;
        case 1:
            SortByDstAddr(netdata);
            break ;
        case 2:
            SortBySrcPort(netdata) ;
            break ;
        case 3:
            SortByDstPort(netdata) ;
            break ;
        default:
            break ;

    } ;
    FillTable() ;
}


void MainWindow::on_openFile_clicked()
{
    on_actionLoad_Network_Data_triggered() ;
}


void MainWindow::on_closeFile_clicked()
{
    QString fname = QFileDialog::getSaveFileName(this, tr("Choose a file"), QDir::currentPath()) ;


    if (!fname.isEmpty())
    {
        if(!SaveFile(fname.toStdString(), netdata))
            return ;
    }
}
