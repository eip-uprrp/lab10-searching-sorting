#Lab. 10: Algoritmos de búsqueda y ordenamiento

Dos tareas comunes cuando se trabaja con arreglos de datos son el buscar datos y el organizar los datos usando algún orden, ascendente o descendente, alfabéticamente o numéricamente. Para realizar estas tareas eficientemente se siguen algoritmos de búsqueda y de ordenamiento. Un algoritmo sencillo para hacer búsquedas es el de búsqueda lineal. Dos algoritmos de ordenamiento sencillos y bien conocidos son el ordenamiento de selección (Selection sort) y el ordenamiento por burbujas (Bubble sort). 

##Objetivos:

En esta experiencia de laboratorio los estudiantes trabajarán en una aplicación para el monitoreo de flujo en redes. El trabajar en esta aplicación les permitirá practicar el uso de una modificación del algoritmo de búsqueda lineal. También reforzarán su conocimiento e implantarán los algoritmos de ordenamiento por selección y de burbuja y practicarán el uso de objetos, estructuras de decisión y repetición, y conocerán sobre el uso de algunos métodos de la clase `vector` de C++.



##Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los algoritmos de búsqueda lineal, ordenamiento por selección y de burbuja.

2. estudiar el método  `size()` de la clase `vector` de C++.

3. familiarizarse con los métodos de la clase `Packet` incluida en el archivo `packet.h` del proyecto `NetworkAnalyzer`.

4. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

5. haber tomado el [Quiz Pre-Lab 10](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6864) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).



##Comunicación entre computadoras

Las computadoras se comunican por medio del Internet utilizando el *Protocolo de Internet* (IP, por sus siglas en inglés). Cuando una computadora envía información (o mensaje) a otra computadora, la información se envía por *Paquetes de Internet* que contienen la *dirección fuente* ("source address"), que es la dirección de Internet de la computadora que está enviando la información, y la *dirección del destino* ("destination address"), que es dirección de Internet de la computadora que debe recibir el mensaje. Las direcciones de Internet se usan para guiar la información de una computadora a otra, pero, una vez el paquete llega a su destino, ?quién se supone que reciba la información? ?Cuál aplicación debe recibir la información?

En las computadoras, las aplicaciones envían y reciben información por lo que en el paquete de Internet también se debe especificar, además de la dirección fuente y la dirección del destino, la aplicación que envía la información y la aplicación que debe recibirla. Podemos pensar en las direcciones de Internet como las direcciones de correo de una casa, y las aplicaciones que envían y reciben como las personas que envían correspondencia y los habitantes de la casa. Hay que especificar a qué persona se le está enviando la carta por correo. Para identificar la aplicación fuente y la aplicación del destino, el protocolo de Internet usa lo que se conoce como *números de puerto*. De este modo, mirando la información del paquete, se puede identificar las direcciones y puertos de la fuente y del destino.

Por ejemplo, cuando la computadora que usas en el laboratorio se comunica con el servidor donde se encuentra Moodle, los paquetes que llevan la información de tu computadora al servidor contienen la dirección de la fuente, que es la computadora del laboratorio, y la dirección del destinatario, que es el servidor de Moodle. El puerto fuente es el de tu buscador web y el puerto destinatario es el de Moodle.
 
Las direcciones de internet ocupan 4 bytes (32 bits) y usualmente se presentan al usuario como cadenas de 4 valores decimales. Cada valor decimal es la representación decimal  de uno de los 4 bytes:
 "(0-255).(0-255).(0-255).(0-255)". Algunos ejemplos de direcciones de IP son:
 10.0.1.10, 192.168.10.11, 136.145.54.10.

Los números de puertos ocupan 2 bytes (16 bits). Por lo tanto, los valores para los números de puertos van de 0 a 65535. Algunos números de puertos asignados a aplicaciones de servicios conocidos son: 22 para `ssh`, 23 para `telnet`, 25 para `smtp`, 80 para `http`.   

La aplicación que veremos hoy se puede utilizar para monitorear lo que se conoce como flujo en redes o "NetFlows". Un "NetFlow" se compone al unir los paquetes de una comunicación unidireccional entre las aplicaciones de dos computadoras. Por ejemplo, un "NetFlow" se puede componer de los paquetes usados para enviar la información desde tu buscador web a la aplicación `http` del servidor de Moodle.

Observa la siguiente foto de la aplicación:

<div align='center'><img src="http://imgur.com/7a1LT8m.png"></div>

**Figura 1.** Ventana para manejar la aplicación de *Network Analyzer*.


Cada fila en la tabla de la Figura 1 contendrá un "NetFlow" compuesto de las direcciones de la fuente y del destinatario, los puertos de la fuente y del destinatario, el número de paquetes y el número de octetos (8 bits) en una comunicación unidireccional entre las coputadoras fuente y destinataria, desde el puerto fuente al puerto destino. 

La aplicación que completarás hoy le permitirá al usuario el analizar el estatus de una red. Entre otras cosas, le permitirá:

* identificar cuáles comunicaciones transmiten la mayor cantidad de datos
* cuáles aplicaciones están corriendo en ciertas computadoras
* cuáles computadoras transmiten grandes cantidades de paquetes comparadas con la cantidad de datos


##Bibliotecas

Para esta experiencia de laboratorio utilizarás objetos de la clase `vector`, que se parece a los arreglos, y necesitarás saber como usar la función `size()` de vectores en C++:


También debes familiarizarte con la biblioteca de la clase `Packet` que se define en este proyecto. La biblioteca `Packet.h` contiene los prototipos de los "setters" y "getters" necesarios para completar la información de un paquete de "NetFlow".


##Sesión de laboratorio




La aplicación en la que trabajarás hoy le permite al usuario subir un archivo que contenga expedientes de "NetFlow" utilizando el botón "Open NetFlow File", lo guarda en un vector de paquetes, y los despliega en la tabla de contenido de la aplicación como se muestra en la Figura 2.

<div align='center'><img src="http://imgur.com/zttw7ep.png"></div>

**Figura 2.** Ventana de la aplicación *Network Analyzer* con los paquetes de  flujo de datos en una red.

El archivo que utilizarás para los ejercicios, `network_sample.dat` contiene expedientes de paquetes de "NetFlow" con el siguiente formato:



```
Source_Address Destination_Address Source_Port Destination_Port Octects Packets
```


```
136.145.181.130 136.145.181.227 5 33 764 16
136.145.181.101 136.145.181.213 37 40 48 4
136.145.181.151 136.145.181.60 45 21 316 9
136.145.181.165 136.145.181.19 8 39 795 24
136.145.181.53 136.145.181.174 34 21 79 22
136.145.181.40 136.145.181.140 58 22 186 5
136.145.181.33 136.145.181.209 76 25 614 13
136.145.181.175 136.145.181.38 30 39 100 8
136.145.181.126 136.145.181.99 57 33 965 14
```


1.  Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab10-searching-sorting.git` para descargar la carpeta `Lab10-Searching-Sorting` a tu computadora. La carpeta `NetworkAnalyzer` contiene el esqueleto de una aplicación para hacer analizar el fujo de data de redes. 

2.  Marca doble "click" en el archivo `NetworkAnalyzer.pro` para cargar este proyecto a Qt.

### **Ejercicio 1: Filtrar comunicaciones**

En este ejercicio completarás las funciones:

* `FilterBySrcAddr`
* `FilterByDstAddr`
* `FilterBySrcPort`
* `FilterByDstPort`

que están contenidas en el archivo `Filter.cpp`.


Cada una de esas funciones recibe un vector de la clase `Packet` y una clave de búsqueda. Cada función (nota sus nombres) está relacionada a un miembro de la clase `Packet`  y deberá "filtrar" los paquetes del vector que correspondan 
a la clave. Para filtrar estos paquetes usarás una modificación del algoritmo de búsqueda lineal que consiste en hacer una búsqueda secuencial para encontrar todas las ocurrencias de un dato.  En cada función, el algoritmo debe buscar en todos los paquetes del vector y desactivar los paquetes en los que el contenido del miembro de interés no es igual al de la clave de búsqueda. Para desactivar el paquete usa la función `disable()` (ver archivo `Packet.cpp`). El filtrado consiste en mantener solo los paquetes que corresponden a la clave.



Por ejemplo, si estamos filtrando por `Source Address` y la clave de búsqueda es 136.145.181.130, la función `FilterBySrcAddr` mantendrá todos los paquetes del vector cuyo `Source Address` es 136.145.181.130 y desactivará todos los otros.

La siguiente figura es una foto de la ventana de la aplicación luego de filtrar los datos por `Source Adrress` con la clave 136.145.181.130.


<div align='center'><img src="http://imgur.com/gfClIOm.png"></div>

**Figura 3.** Ventana de la aplicación *Network Analyzer* con los paquetes de  flujo de datos en una red filtrados por `Source Address` con clave 136.145.181.130.


**Entrega 1**

Sube un archivo con las funciones que desarrollaste para hacer los filtros a la página [Entrega 1 del Lab 10](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=6865). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.

### **Ejercicio 2: Ordenar datos**

En este ejercicio completarás las funciones:

* `SortBySrcAddr`
* `SortByDstAddr`
* `SortBySrcPort`
* `SortByDstPort`

que están contenidas en el archivo `Sort.cpp`.

Cada una de esas funciones recibe un vector  de la clase `Packet`. Cada función (nota sus nombres) está relacionada a un miembro de la clase `Packet`  y deberá "ordenar" los paquetes del vector de acuerdo al miembro de interés.

La siguiente figura es una foto de la ventana de la aplicación luego de ordenar los datos por `Source Address`.


<div align='center'><img src="http://imgur.com/wR4mzKg.png"> </div>

**Figura 4.** Ventana de la aplicación *Network Analyzer* con los paquetes de  flujo de datos en una red ordenados por `Source Address`

 

1. Completa la función `SortBySrcAddr` implementando el algoritmo de burbuja (*Bubble Sort*), ordenando los paquetes por el `Source address`. 
2. Completa la función `SortByDstAddr` implementando el algoritmo de selección (*Selection Sort*), ordenando los paquetes por el `Destination address`.
3. Completa la función `SortBySrcPort` implementando el algoritmo de selección (*Selection Sort*), ordenando los paquetes por el `Source port`.
4. Completa la función `SortByDstPort` implementando el algoritmo de burbuja (*Bubble Sort*), ordenando los paquetes por el `Destination port`. 


**Entrega 2**

Sube un archivo con las funciones que desarrollaste para ordenar los datos a la  página  [Entrega 2 del Lab 10](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=6866). Recuerda comentar adecuadamente la función y usar buenas prácticas de indentación y selección de nombres para las variables.


